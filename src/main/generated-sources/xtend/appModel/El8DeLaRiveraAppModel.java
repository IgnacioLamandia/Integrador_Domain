package appModel;

import java.util.Date;
import java.util.List;
import model.Apuesta;
import model.Repositorio;
import model.Resultado;
import model.Ruleta;
import model.Tendencia;
import model.Usuario;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class El8DeLaRiveraAppModel {
  private Repositorio repositorio;
  
  private Ruleta ruleta;
  
  public El8DeLaRiveraAppModel(final Repositorio repositorio) {
    this.repositorio = repositorio;
    Ruleta _ruleta = new Ruleta();
    this.ruleta = _ruleta;
  }
  
  public List<Apuesta> getTotalApuetas() {
    return this.repositorio.getApuestas();
  }
  
  public List<Tendencia> getTotalTendencias() {
    return this.repositorio.getTendencias();
  }
  
  public List<Apuesta> getApuetasUsuario(final String nombre) {
    Usuario _usuario = this.repositorio.getUsuario(nombre);
    return _usuario.getApuestas();
  }
  
  public Usuario logearUsuario(final String nombre, final String password) {
    return this.repositorio.logearUsuario(nombre, password);
  }
  
  public boolean apostar(final String nombre, final String monto, final String numero) {
    boolean _xblockexpression = false;
    {
      Apuesta apuesta = this.repositorio.nuevaApuesta(nombre, monto, numero);
      Usuario _usuario = this.repositorio.getUsuario(nombre);
      this.ruleta.chequearApuesta(apuesta, _usuario);
      List<Tendencia> _tendencias = this.repositorio.getTendencias();
      Date _fecha = apuesta.getFecha();
      Resultado _resultado = apuesta.getResultado();
      int _ganador = _resultado.getGanador();
      Tendencia _tendencia = new Tendencia(_fecha, _ganador);
      _xblockexpression = _tendencias.add(_tendencia);
    }
    return _xblockexpression;
  }
  
  public int getSaldoUsuario(final String nombre) {
    Usuario _usuario = this.repositorio.getUsuario(nombre);
    return _usuario.getSaldo();
  }
  
  @Pure
  public Repositorio getRepositorio() {
    return this.repositorio;
  }
  
  public void setRepositorio(final Repositorio repositorio) {
    this.repositorio = repositorio;
  }
  
  @Pure
  public Ruleta getRuleta() {
    return this.ruleta;
  }
  
  public void setRuleta(final Ruleta ruleta) {
    this.ruleta = ruleta;
  }
}
