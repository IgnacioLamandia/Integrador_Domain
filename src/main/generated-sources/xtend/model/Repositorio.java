package model;

import com.google.common.base.Objects;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Apuesta;
import model.Resultado;
import model.Tendencia;
import model.Usuario;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.model.UserException;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class Repositorio {
  private List<Usuario> usuarios;
  
  private List<Apuesta> apuestas;
  
  private List<Tendencia> tendencias;
  
  private List<Usuario> usuariosActivos;
  
  private Usuario usuario;
  
  public Repositorio(final List<Usuario> usuarios) {
    this.usuarios = usuarios;
    ArrayList<Apuesta> _arrayList = new ArrayList<Apuesta>();
    this.apuestas = _arrayList;
    ArrayList<Tendencia> _arrayList_1 = new ArrayList<Tendencia>();
    this.tendencias = _arrayList_1;
    ArrayList<Usuario> _arrayList_2 = new ArrayList<Usuario>();
    this.usuariosActivos = _arrayList_2;
  }
  
  public Repositorio(final List<Usuario> usuarios, final List<Apuesta> apuestas) {
    this.usuarios = usuarios;
    this.apuestas = apuestas;
    ArrayList<Tendencia> _arrayList = new ArrayList<Tendencia>();
    this.tendencias = _arrayList;
    ArrayList<Usuario> _arrayList_1 = new ArrayList<Usuario>();
    this.usuariosActivos = _arrayList_1;
  }
  
  public Repositorio(final List<Usuario> usuarios, final List<Apuesta> apuestas, final List<Tendencia> tendencias) {
    this.usuarios = usuarios;
    this.apuestas = apuestas;
    this.tendencias = tendencias;
    ArrayList<Usuario> _arrayList = new ArrayList<Usuario>();
    this.usuariosActivos = _arrayList;
  }
  
  public Usuario getUsuario(final String nombre) {
    final Function1<Usuario, Boolean> _function = new Function1<Usuario, Boolean>() {
      public Boolean apply(final Usuario it) {
        String _nombre = it.getNombre();
        String _lowerCase = _nombre.toLowerCase();
        String _lowerCase_1 = nombre.toLowerCase();
        return Boolean.valueOf(_lowerCase.equals(_lowerCase_1));
      }
    };
    return IterableExtensions.<Usuario>findFirst(this.usuarios, _function);
  }
  
  public boolean existeUsuario(final String nombre) {
    final Function1<Usuario, Boolean> _function = new Function1<Usuario, Boolean>() {
      public Boolean apply(final Usuario it) {
        String _nombre = it.getNombre();
        String _lowerCase = _nombre.toLowerCase();
        String _lowerCase_1 = nombre.toLowerCase();
        return Boolean.valueOf(_lowerCase.equals(_lowerCase_1));
      }
    };
    return IterableExtensions.<Usuario>exists(this.usuarios, _function);
  }
  
  public boolean agregarUsuario(final String nombre, final String pass) {
    boolean _xifexpression = false;
    boolean _existeUsuario = this.existeUsuario(nombre);
    boolean _not = (!_existeUsuario);
    if (_not) {
      boolean _xblockexpression = false;
      {
        final Usuario nuevoUsuario = new Usuario(nombre, pass);
        _xblockexpression = this.usuarios.add(nuevoUsuario);
      }
      _xifexpression = _xblockexpression;
    } else {
      throw new UserException("El nombre no esta disponible");
    }
    return _xifexpression;
  }
  
  public Usuario logearUsuario(final String nombre, final String password) {
    Usuario _xifexpression = null;
    boolean _existeUsuario = this.existeUsuario(nombre);
    if (_existeUsuario) {
      Usuario _xblockexpression = null;
      {
        Usuario usuario = this.getUsuario(nombre);
        Usuario _xifexpression_1 = null;
        String _password = usuario.getPassword();
        boolean _equals = Objects.equal(_password, password);
        if (_equals) {
          _xifexpression_1 = usuario = usuario;
        } else {
          throw new UserException("password incorrecta");
        }
        _xblockexpression = _xifexpression_1;
      }
      _xifexpression = _xblockexpression;
    } else {
      throw new UserException("Usuario inexistente");
    }
    return _xifexpression;
  }
  
  public Apuesta nuevaApuesta(final String nombre, final String m, final String n) {
    Apuesta _xblockexpression = null;
    {
      Usuario usuario = this.getUsuario(nombre);
      int monto = Integer.parseInt(m);
      int numero = Integer.parseInt(n);
      Apuesta _xifexpression = null;
      if (((((numero >= 0) && 
        (numero <= 36)) && 
        (monto > 0)) && 
        (monto <= usuario.getSaldo()))) {
        Apuesta _xblockexpression_1 = null;
        {
          Apuesta apuesta = new Apuesta(monto, numero);
          List<Apuesta> _apuestas = usuario.getApuestas();
          _apuestas.add(apuesta);
          this.apuestas.add(apuesta);
          int _saldo = usuario.getSaldo();
          int _minus = (_saldo - monto);
          usuario.setSaldo(_minus);
          _xblockexpression_1 = apuesta;
        }
        _xifexpression = _xblockexpression_1;
      } else {
        throw new RuntimeException("Apuesta invalida");
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public List<Tendencia> actualizarTendencias() {
    List<Tendencia> _xblockexpression = null;
    {
      ArrayList<Tendencia> res = new ArrayList<Tendencia>();
      for (final Apuesta a : this.apuestas) {
        Date _fecha = a.getFecha();
        Resultado _resultado = a.getResultado();
        int _ganador = _resultado.getGanador();
        Tendencia _tendencia = new Tendencia(_fecha, _ganador);
        res.add(_tendencia);
      }
      _xblockexpression = this.tendencias = res;
    }
    return _xblockexpression;
  }
  
  @Pure
  public List<Usuario> getUsuarios() {
    return this.usuarios;
  }
  
  public void setUsuarios(final List<Usuario> usuarios) {
    this.usuarios = usuarios;
  }
  
  @Pure
  public List<Apuesta> getApuestas() {
    return this.apuestas;
  }
  
  public void setApuestas(final List<Apuesta> apuestas) {
    this.apuestas = apuestas;
  }
  
  @Pure
  public List<Tendencia> getTendencias() {
    return this.tendencias;
  }
  
  public void setTendencias(final List<Tendencia> tendencias) {
    this.tendencias = tendencias;
  }
  
  @Pure
  public List<Usuario> getUsuariosActivos() {
    return this.usuariosActivos;
  }
  
  public void setUsuariosActivos(final List<Usuario> usuariosActivos) {
    this.usuariosActivos = usuariosActivos;
  }
  
  @Pure
  public Usuario getUsuario() {
    return this.usuario;
  }
  
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }
}
