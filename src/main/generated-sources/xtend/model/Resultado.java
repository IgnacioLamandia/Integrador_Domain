package model;

import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;

@Accessors
@SuppressWarnings("all")
public class Resultado {
  private int ganador;
  
  private String mensaje;
  
  public Resultado(final int ganador) {
    this.ganador = ganador;
  }
  
  @Pure
  public int getGanador() {
    return this.ganador;
  }
  
  public void setGanador(final int ganador) {
    this.ganador = ganador;
  }
  
  @Pure
  public String getMensaje() {
    return this.mensaje;
  }
  
  public void setMensaje(final String mensaje) {
    this.mensaje = mensaje;
  }
}
