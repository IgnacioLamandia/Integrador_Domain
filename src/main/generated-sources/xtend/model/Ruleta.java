package model;

import java.util.Random;
import model.Apuesta;
import model.Ganador;
import model.Perdedor;
import model.Usuario;
import org.uqbar.commons.utils.Observable;

@Observable
@SuppressWarnings("all")
public class Ruleta {
  public boolean esGanador(final int ganador, final int numero) {
    return (ganador == numero);
  }
  
  public void chequearApuesta(final Apuesta apuesta, final Usuario usuario) {
    Random _random = new Random();
    final int ganador = _random.nextInt(37);
    int _numero = apuesta.getNumero();
    boolean _esGanador = this.esGanador(ganador, _numero);
    if (_esGanador) {
      int _monto = apuesta.getMonto();
      int montoGanado = (_monto * 10);
      Ganador _ganador = new Ganador(ganador, montoGanado);
      apuesta.setResultado(_ganador);
      int _saldo = usuario.getSaldo();
      int _plus = (_saldo + montoGanado);
      usuario.setSaldo(_plus);
    } else {
      Perdedor _perdedor = new Perdedor(ganador);
      apuesta.setResultado(_perdedor);
    }
  }
}
