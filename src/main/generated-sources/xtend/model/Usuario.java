package model;

import java.util.ArrayList;
import java.util.List;
import model.Apuesta;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class Usuario {
  private String nombre;
  
  private String password;
  
  private List<Apuesta> apuestas;
  
  private int saldo;
  
  public Usuario(final String nombre, final String password) {
    this.nombre = nombre;
    this.password = password;
    ArrayList<Apuesta> _arrayList = new ArrayList<Apuesta>();
    this.apuestas = _arrayList;
    this.saldo = 0;
  }
  
  @Pure
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }
  
  @Pure
  public String getPassword() {
    return this.password;
  }
  
  public void setPassword(final String password) {
    this.password = password;
  }
  
  @Pure
  public List<Apuesta> getApuestas() {
    return this.apuestas;
  }
  
  public void setApuestas(final List<Apuesta> apuestas) {
    this.apuestas = apuestas;
  }
  
  @Pure
  public int getSaldo() {
    return this.saldo;
  }
  
  public void setSaldo(final int saldo) {
    this.saldo = saldo;
  }
}
