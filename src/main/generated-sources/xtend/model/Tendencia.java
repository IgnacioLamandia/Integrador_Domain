package model;

import java.util.Date;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class Tendencia {
  private Date fecha;
  
  private int numero;
  
  public Tendencia(final Date fecha, final int numero) {
    this.fecha = fecha;
    this.numero = numero;
  }
  
  @Pure
  public Date getFecha() {
    return this.fecha;
  }
  
  public void setFecha(final Date fecha) {
    this.fecha = fecha;
  }
  
  @Pure
  public int getNumero() {
    return this.numero;
  }
  
  public void setNumero(final int numero) {
    this.numero = numero;
  }
}
