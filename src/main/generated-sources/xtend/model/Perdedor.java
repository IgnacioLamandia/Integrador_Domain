package model;

import model.Resultado;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class Perdedor extends Resultado {
  public Perdedor(final int ganador) {
    super(ganador);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Perdiste, salio el ");
    _builder.append(ganador, "");
    this.setMensaje(_builder.toString());
  }
}
