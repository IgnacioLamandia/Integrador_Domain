package model;

import java.util.Date;
import model.Resultado;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class Apuesta {
  private int monto;
  
  private int numero;
  
  private Date fecha;
  
  private Resultado resultado;
  
  public Apuesta(final int monto, final int numero) {
    this.numero = numero;
    this.monto = monto;
    Date _date = new Date();
    this.fecha = _date;
    this.resultado = null;
  }
  
  @Pure
  public int getMonto() {
    return this.monto;
  }
  
  public void setMonto(final int monto) {
    this.monto = monto;
  }
  
  @Pure
  public int getNumero() {
    return this.numero;
  }
  
  public void setNumero(final int numero) {
    this.numero = numero;
  }
  
  @Pure
  public Date getFecha() {
    return this.fecha;
  }
  
  public void setFecha(final Date fecha) {
    this.fecha = fecha;
  }
  
  @Pure
  public Resultado getResultado() {
    return this.resultado;
  }
  
  public void setResultado(final Resultado resultado) {
    this.resultado = resultado;
  }
}
