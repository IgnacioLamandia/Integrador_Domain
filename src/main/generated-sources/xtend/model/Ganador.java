package model;

import model.Resultado;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class Ganador extends Resultado {
  private int montoGanado;
  
  public Ganador(final int ganador, final int montoGanado) {
    super(ganador);
    this.montoGanado = montoGanado;
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Ganaste $");
    _builder.append(montoGanado, "");
    this.setMensaje(_builder.toString());
  }
}
