package appModel

import model.Repositorio
import model.Ruleta
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import model.Tendencia

@Observable
@Accessors
class El8DeLaRiveraAppModel {

    Repositorio repositorio
    Ruleta ruleta
	
	new(Repositorio repositorio){
        this.repositorio = repositorio
        ruleta = new Ruleta()
    }

	def getTotalApuetas(){
		return repositorio.getApuestas() 
	}
	
	def getTotalTendencias(){
		return repositorio.getTendencias() 
	}
	
	def getApuetasUsuario(String nombre) {
		return repositorio.getUsuario(nombre).getApuestas() 
	}
	
	def logearUsuario(String nombre, String password){
		this.repositorio.logearUsuario(nombre, password)	
	}

	def apostar(String nombre, String monto, String numero){
		var apuesta = repositorio.nuevaApuesta(nombre, monto, numero)
		ruleta.chequearApuesta(apuesta, repositorio.getUsuario(nombre))
		repositorio.tendencias.add(new Tendencia(apuesta.fecha, apuesta.resultado.ganador))		
	}
	
	def getSaldoUsuario(String nombre) {
		return this.repositorio.getUsuario(nombre).getSaldo()
	}
	
}
