package model

import java.util.ArrayList
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class Usuario {
	
	String nombre
	String password
	List<Apuesta> apuestas
	int saldo
	
	new(String nombre, String password)
	{
		this.nombre = nombre
		this.password = password
		this.apuestas = new ArrayList<Apuesta>()
		this.saldo = 0
	}
}