package model

import java.util.Date
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class Apuesta {
	
    int monto
    int numero
    Date fecha
    Resultado resultado
    
    new(int monto, int numero){
        this.numero = numero
        this.monto = monto
        this.fecha = new Date()
        this.resultado = null
    }

}