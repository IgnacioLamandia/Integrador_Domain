package model

import java.util.List
import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.UserException
import java.util.ArrayList

@Observable
@Accessors
class Repositorio{
	
	List<Usuario> usuarios;
	List<Apuesta> apuestas;
	List<Tendencia> tendencias;
	List<Usuario> usuariosActivos;
	Usuario usuario
	
    new(List<Usuario> usuarios) {
        this.usuarios = usuarios;
        this.apuestas = new ArrayList<Apuesta>();
        this.tendencias = new ArrayList<Tendencia>();
        this.usuariosActivos = new ArrayList<Usuario>();
    }
    
    new(List<Usuario> usuarios, List<Apuesta> apuestas) {
        this.usuarios = usuarios;
        this.apuestas = apuestas;
        this.tendencias = new ArrayList<Tendencia>();
        this.usuariosActivos = new ArrayList<Usuario>();
    }
    
    new(List<Usuario> usuarios, List<Apuesta> apuestas, List<Tendencia> tendencias) {
        this.usuarios = usuarios;
        this.apuestas = apuestas;
        this.tendencias = tendencias;
        this.usuariosActivos = new ArrayList<Usuario>();
    }

    def Usuario getUsuario(String nombre) {
        return usuarios.findFirst [ (it.nombre.toLowerCase()).equals(nombre.toLowerCase())]
    }
    
    def existeUsuario(String nombre) {
        usuarios.exists [ (it.nombre.toLowerCase()).equals(nombre.toLowerCase())]
    }

	def agregarUsuario(String nombre, String pass)
	{
		if(!existeUsuario(nombre)){
			val nuevoUsuario = new Usuario(nombre, pass)
        	usuarios.add(nuevoUsuario)
        }
        else{
        	throw new UserException("El nombre no esta disponible")
        }
	}
	
	def Usuario logearUsuario(String nombre, String password)
	{
		if(this.existeUsuario(nombre))
		{
			var usuario = getUsuario(nombre)
			if(usuario.password == password)
			{
        		//this.usuariosActivos.add(usuario);
        		usuario = usuario
        	}
        	else throw new UserException("password incorrecta")
        }
		else throw new UserException("Usuario inexistente")
	}

	
	def Apuesta nuevaApuesta(String nombre, String m, String n){
 	
 	var usuario = this.getUsuario(nombre)
 	var monto = Integer.parseInt(m)
 	var numero = Integer.parseInt(n)
 	
 	if( numero >= 0  && 
    	numero <= 36 && 
    	monto  >  0  && 
    	monto  <=  usuario.saldo)
    	{	
    		var apuesta = new Apuesta(monto, numero)
 			usuario.apuestas.add(apuesta)
 			this.apuestas.add(apuesta)
    		usuario.saldo = usuario.saldo - monto
    		apuesta
 		}
 		
 		else throw new RuntimeException("Apuesta invalida")
	}

	def actualizarTendencias()
	{
		var res = new ArrayList<Tendencia>() 	
    	for(a : this.apuestas)
    	{
    		res.add(new Tendencia(a.fecha, a.resultado.ganador))
    	}
    	
    	this.tendencias = res	    	
	}
	
}