package model

import java.util.Random
import org.uqbar.commons.utils.Observable

@Observable
class Ruleta {

	def esGanador(int ganador, int numero) {
		ganador == numero
	}

	def chequearApuesta(Apuesta apuesta, Usuario usuario) {
		
		val ganador = new Random().nextInt(37)
		
		if (esGanador(ganador, apuesta.numero)){
			var int montoGanado = apuesta.monto * 10
			apuesta.resultado = new Ganador(ganador, montoGanado)
			usuario.saldo = usuario.saldo + montoGanado}
		else
			apuesta.resultado = new Perdedor(ganador)
	}	
}
