package model

import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class Resultado {
	
	int ganador
	String mensaje

	new(int ganador) {
		this.ganador = ganador
	}
}

class Ganador extends Resultado {
	
	int montoGanado

	new(int ganador, int montoGanado) {
		super(ganador)
		this.montoGanado = montoGanado
		mensaje = '''Ganaste $«montoGanado»'''
	}

}

class Perdedor extends Resultado {
	
	new(int ganador) {
		super(ganador)
		mensaje = '''Perdiste, salio el «ganador»'''
	}

}
