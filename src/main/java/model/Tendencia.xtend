package model

import java.util.Date
import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors

@Observable
@Accessors
class Tendencia {
	
	Date fecha
	int numero
	
	new(Date fecha, int numero)
	{
		this.fecha = fecha
		this.numero = numero
	}
	
}